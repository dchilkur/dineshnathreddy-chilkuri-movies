package com.dc.movies.repository.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dc.movies.entity.PurchaseHistory;
import com.dc.movies.entity.User;
import com.dc.movies.entity.impl.UserImpl;
import com.dc.movies.repository.UserServiceRepository;
import com.dc.movies.service.exception.CannotFindUserException;
@Repository
public class UserServiceRepositoryImpl implements UserServiceRepository{
	
	@Autowired
    private SessionFactory sessionFactory;
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public User getUser(String userId) {
		// TODO Auto-generated method stub
		
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(UserImpl.class)
				.add(Restrictions.eq("userName", userId));	
		List<User> users=criteria.list();
		if(!(users.size()>0)){
			throw new CannotFindUserException("Cannot find the Associate User");
		}
		return users.get(0);

	}

	@Override
	public String registerUser(User user) {
		// TODO Auto-generated method stub
		return (String) this.sessionFactory.getCurrentSession().save(user);
	}

	@Override
	public PurchaseHistory getPreviousPurchaseHistory() {
		// TODO Auto-generated method stub
		return null;
	}

}
