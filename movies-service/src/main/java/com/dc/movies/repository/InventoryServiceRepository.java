package com.dc.movies.repository;

import java.util.List;

import com.dc.movies.entity.Inventory;
import com.dc.movies.entity.Movie;
import com.dc.movies.entity.Theather;
import com.dc.movies.service.exception.CannotFindMovieException;
import com.dc.movies.service.exception.CannotFindTheatherException;
import com.dc.movies.service.exception.NoMoviesCurrentlyPlayingException;
import com.dc.movies.service.exception.NoTheathresPlayingThisMovieException;

public interface InventoryServiceRepository {
	
	List<Inventory> getMovieListings();
	List<Movie> getAllMovies(String theather_id) throws CannotFindMovieException, NoMoviesCurrentlyPlayingException;
	List<Theather> getAllTheathers(String movie_id) throws CannotFindTheatherException, NoTheathresPlayingThisMovieException;
    int addInventory(Inventory Inventory);
    Inventory getInventory(Integer id);
	List<Inventory> getAllInventoryByTheather(String theather_id);
	List<Inventory> getAllInventoryByMovie(String movie_id);


}
