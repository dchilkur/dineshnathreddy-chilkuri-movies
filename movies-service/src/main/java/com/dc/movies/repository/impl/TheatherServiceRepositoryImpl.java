package com.dc.movies.repository.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dc.movies.entity.Theather;
import com.dc.movies.entity.impl.TheathreImpl;
import com.dc.movies.repository.TheatherServiceRepository;
import com.dc.movies.service.exception.CannotFindTheatherException;
import com.dc.movies.service.exception.NoMatchingTheathersFoundException;
import com.dc.movies.service.exception.NoTheathersAvailableException;

@Repository
public class TheatherServiceRepositoryImpl implements TheatherServiceRepository{
	
	
	@Autowired
    private SessionFactory sessionFactory;

	@Override
	public String addTheather(Theather theather) {
		// TODO Auto-generated method stub
		return (String) this.sessionFactory.getCurrentSession().save(theather);
		
	}

	@Override
	public void removeTheather(Theather theather) {
		// TODO Auto-generated method stub

		this.sessionFactory.getCurrentSession().delete(theather);
		
	}


	@Override
	public List<Theather> getAllTheathres()
			throws NoTheathersAvailableException {
		// TODO Auto-generated method stub
		
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TheathreImpl.class);	
		List<Theather> theathers=criteria.list();
		if(!(theathers.size()>0)){
			throw new NoTheathersAvailableException();
		}
		return theathers;
	}

	@Override
	public Theather getTheather(String string)
			throws CannotFindTheatherException {
		// TODO Auto-generated method stub
		
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TheathreImpl.class)
				.add(Restrictions.eq("theatherID", string));	
		List<Theather> theathers=criteria.list();
		if(!(theathers.size()>0)){
			throw new CannotFindTheatherException("No Matching Theathers Found");
		}
		return theathers.get(0);
		
	}

	@Override
	public List<Theather> getAllTheathres(String string) throws NoMatchingTheathersFoundException {
		// TODO Auto-generated method stub
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(TheathreImpl.class)
				.add(Restrictions.like("theather_name", string+"%") );	
		List<Theather> theathers=criteria.list();
		if(!(theathers.size()>0)){
			throw new NoMatchingTheathersFoundException();
		}
		return theathers;
	}
	
	

}
