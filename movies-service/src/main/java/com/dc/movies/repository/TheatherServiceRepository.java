package com.dc.movies.repository;

import java.util.List;

import com.dc.movies.entity.Theather;
import com.dc.movies.service.exception.CannotFindTheatherException;
import com.dc.movies.service.exception.NoMatchingTheathersFoundException;
import com.dc.movies.service.exception.NoTheathersAvailableException;

public interface TheatherServiceRepository {
	
	String addTheather(Theather theather);
	void removeTheather(Theather theather);
	List<Theather> getAllTheathres() throws NoTheathersAvailableException;
	Theather getTheather(String string) throws CannotFindTheatherException;
	List<Theather> getAllTheathres(String string) throws  NoMatchingTheathersFoundException;

}
