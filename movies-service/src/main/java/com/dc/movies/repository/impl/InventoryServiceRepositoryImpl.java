package com.dc.movies.repository.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dc.movies.entity.Inventory;
import com.dc.movies.entity.Movie;
import com.dc.movies.entity.Theather;
import com.dc.movies.entity.impl.InventoryImpl;
import com.dc.movies.repository.InventoryServiceRepository;
import com.dc.movies.service.exception.CannotFindMovieException;
import com.dc.movies.service.exception.CannotFindTheatherException;
import com.dc.movies.service.exception.NoMoviesCurrentlyPlayingException;
import com.dc.movies.service.exception.NoTheathresPlayingThisMovieException;

@Repository
public class InventoryServiceRepositoryImpl implements InventoryServiceRepository {
	
	
	@Autowired
    private SessionFactory sessionFactory;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Override
	public int addInventory(Inventory inventory) {
		
		logger.info("in Add inventory method - Repository Impl");
		// TODO Auto-generated method stub
		return (Integer) this.sessionFactory.getCurrentSession().save(inventory);
	}

	@Override
	public List<Inventory> getMovieListings() {
		// TODO Auto-generated method stub
		
		
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(InventoryImpl.class);	
		List<Inventory> inventory=criteria.list();
		return inventory;
	}

	@Override
	public List<Movie> getAllMovies(String theather_id)
			throws CannotFindMovieException, NoMoviesCurrentlyPlayingException {
		// TODO Auto-generated method stub
		
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(InventoryImpl.class).add(Restrictions.eq("theather.theatherID", theather_id));	
		List<Inventory> inventory=criteria.list();
		
		if(!(inventory.size()>0)){
			throw new NoMoviesCurrentlyPlayingException();
		}
		List<Movie> moviesList=new ArrayList<Movie>();
		for (Inventory element : inventory) {
			moviesList.add(element.getMovie());
			
		}
		
		return moviesList;
	}

	@Override
	public List<Theather> getAllTheathers(String movie_id)
			throws CannotFindTheatherException,
			NoTheathresPlayingThisMovieException {
		// TODO Auto-generated method stub
		
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(InventoryImpl.class).add(Restrictions.eq("movie.movie_id", movie_id));	
		List<Inventory> inventory=criteria.list();
		
		if(!(inventory.size()>0)){
			throw new NoTheathresPlayingThisMovieException();
		}
		List<Theather> theathersList=new ArrayList<Theather>();
		for (Inventory element : inventory) {
			theathersList.add(element.getTheather());
		}
		
		return theathersList;
	}

	@Override
	public Inventory getInventory(Integer id) {
		// TODO Auto-generated method stub
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(InventoryImpl.class).add(Restrictions.eq("inventory_id", id));
		List<Inventory> inventory=criteria.list();
		return inventory.get(0);
	}

	@Override
	public List<Inventory> getAllInventoryByTheather(String theather_id) {
		// TODO Auto-generated method stub
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(InventoryImpl.class).add(Restrictions.eq("theather.theatherID", theather_id));	
		List<Inventory> inventory=criteria.list();
		return inventory;
	}

	@Override
	public List<Inventory> getAllInventoryByMovie(String movie_id) {
		// TODO Auto-generated method stub
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(InventoryImpl.class).add(Restrictions.eq("movie.movie_id", movie_id));	
		List<Inventory> inventory=criteria.list();
		return inventory;
	}



	

}
