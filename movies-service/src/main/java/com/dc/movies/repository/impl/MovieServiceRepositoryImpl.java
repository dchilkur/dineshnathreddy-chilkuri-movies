package com.dc.movies.repository.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dc.movies.entity.Movie;
import com.dc.movies.entity.impl.MovieImpl;
import com.dc.movies.repository.MovieServiceRepository;
import com.dc.movies.service.exception.CannotFindMovieException;
import com.dc.movies.service.exception.NoMoviesAvailableException;


@Repository
public class MovieServiceRepositoryImpl implements MovieServiceRepository{
	
	@Autowired
    private SessionFactory sessionFactory;

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Override
	public String addMovie(Movie movie) {
		// TODO Auto-generated method stub
		logger.info("Release Date is "+movie.getRelease_date());
		return (String) this.sessionFactory.getCurrentSession().save(movie);
	}

	@Override
	public  void deleteMovie(Movie movie) {
		// TODO Auto-generated method stub
		 this.sessionFactory.getCurrentSession().delete(movie);
	}

	@Override
	public Movie getMovie(String movie_id) throws CannotFindMovieException {
		// TODO Auto-generated method stub
		
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(MovieImpl.class)
				.add(Restrictions.eq("movie_id", movie_id));	
		List<Movie> movies=criteria.list();
		if(!(movies.size()>0)){
			throw new CannotFindMovieException("Cannot find the Associate Movie");
		}
		return movies.get(0);
	}

	@Override
	public List<Movie> getAllMovies() throws NoMoviesAvailableException {
		// TODO Auto-generated method stub
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(MovieImpl.class);
		List<Movie> movies=criteria.list();
		if(!(movies.size()>0)){
			throw new NoMoviesAvailableException();
		
		}
		return movies;
	}

	@Override
	public List<Movie> getAllMovies(String string) {
		// TODO Auto-generated method stub
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(MovieImpl.class)
				.add(Restrictions.like("movie_name", string+"%"));	
		List<Movie> movies=criteria.list();
		return movies;
	}

}
