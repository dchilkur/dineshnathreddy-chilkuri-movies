package com.dc.movies.repository;

import java.util.List;

import com.dc.movies.entity.Movie;
import com.dc.movies.service.exception.CannotFindMovieException;
import com.dc.movies.service.exception.NoMoviesAvailableException;



public interface MovieServiceRepository {
	
	String addMovie(Movie movie);
	void deleteMovie(Movie movie);	
	Movie getMovie(String movie_id) throws CannotFindMovieException;
	List<Movie> getAllMovies() throws NoMoviesAvailableException;
	List<Movie> getAllMovies(String string);

}
