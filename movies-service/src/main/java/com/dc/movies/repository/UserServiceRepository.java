package com.dc.movies.repository;

import com.dc.movies.entity.PurchaseHistory;
import com.dc.movies.entity.User;

public interface UserServiceRepository {

	
	User getUser(String userId);
	String registerUser(User user);
	PurchaseHistory getPreviousPurchaseHistory();

}
