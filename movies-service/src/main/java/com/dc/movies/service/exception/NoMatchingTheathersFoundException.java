package com.dc.movies.service.exception;

public class NoMatchingTheathersFoundException extends MoviesException{
	// Parameterless Constructor
	public NoMatchingTheathersFoundException() {
	}

	// Constructor that accepts a message
	public NoMatchingTheathersFoundException(String message) {
		super(ErrorCode.INVALID_FIELD, message);
	}
	
    public NoMatchingTheathersFoundException(String message, Throwable throwable) {
		super(ErrorCode.INVALID_FIELD, message, throwable);
	}
    

}

