package com.dc.movies.service;

import com.dc.movies.entity.PurchaseHistory;
import com.dc.movies.entity.User;



public interface UserService {
	
	User getUser(String userId);
	String registerUser(User user);
	PurchaseHistory getPreviousPurchaseHistory();

}
