package com.dc.movies.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dc.movies.entity.Movie;
import com.dc.movies.repository.MovieServiceRepository;
import com.dc.movies.service.MovieService;
import com.dc.movies.service.exception.CannotFindMovieException;
import com.dc.movies.service.exception.NoMatchingMoviesFoundException;
import com.dc.movies.service.exception.NoMoviesAvailableException;
import com.dc.movies.util.MovieUtil;

@Service
@Transactional
public class MovieServiceImpl implements MovieService {
	
	@Autowired
	private MovieServiceRepository movieServiceRepository;

	@Override
	public String addMovie(Movie movie) {
		// TODO Auto-generated method stub
		String id=movieServiceRepository.addMovie(movie);
		return id;

	}

	@Override
	public void removeMovie(Movie movie) {
		// TODO Auto-generated method stub
		movieServiceRepository.deleteMovie(movie);

	}

	@Override
	public void updateMovie(Movie movie) {
		// TODO Auto-generated method stub

	}

	@Override
	public Movie getMovie(String movie_id) throws CannotFindMovieException {
		// TODO Auto-generated method stub

		Movie returnMovie=movieServiceRepository.getMovie(movie_id);

		return returnMovie;
	}

	@Override
	public List<Movie> getAllMovies() throws NoMoviesAvailableException{
		// TODO Auto-generated method stub
		List<Movie> movies = movieServiceRepository.getAllMovies();

		if (!(movies.size() > 0)) {
			throw new NoMoviesAvailableException("No Movies Available at this time");
		}

		return movies;
	}

	@Override
	public List<Movie> getAllMovies(String movieName)
			throws NoMatchingMoviesFoundException {
		// TODO Auto-generated method stub

		List<Movie> movies = movieServiceRepository.getAllMovies(movieName);
		if (!(movies.size() > 0)) {
			throw new NoMoviesAvailableException("No Movies Available at this time");
		}

		return movies;
	}

}
