package com.dc.movies.service.exception;

public class NoMoviesCurrentlyPlayingException extends MoviesException{
	// Parameterless Constructor
	public NoMoviesCurrentlyPlayingException() {
	}

	// Constructor that accepts a message
	public NoMoviesCurrentlyPlayingException(String message) {
		super(ErrorCode.INVALID_FIELD, message);
	}
	
	 public NoMoviesCurrentlyPlayingException(String message, Throwable throwable) {
			super(ErrorCode.INVALID_FIELD, message, throwable);
		}

}
