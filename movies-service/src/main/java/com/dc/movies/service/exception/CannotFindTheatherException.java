package com.dc.movies.service.exception;

public class CannotFindTheatherException  extends MoviesException{

	    //Parameterless Constructor
	    public CannotFindTheatherException() {}

	    //Constructor that accepts a message
	    public CannotFindTheatherException(String message)
	    {
	       super(ErrorCode.INVALID_FIELD, message);
	    }
	    
	    public CannotFindTheatherException(String message, Throwable throwable) {
			super(ErrorCode.INVALID_FIELD, message, throwable);
		}
	    


}
