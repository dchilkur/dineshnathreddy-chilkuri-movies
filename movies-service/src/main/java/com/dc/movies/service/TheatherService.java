package com.dc.movies.service;

import java.util.List;


import com.dc.movies.entity.Theather;
import com.dc.movies.service.exception.CannotFindTheatherException;
import com.dc.movies.service.exception.NoMatchingTheathersFoundException;
import com.dc.movies.service.exception.NoTheathersAvailableException;

public interface TheatherService {
	
	String addTheather(Theather theather);
	void removeTheather(Theather theather);
	void updateTheather(Theather theather);
	List<Theather> getAllTheathres() throws NoTheathersAvailableException;
	List<Theather> getAllTheathres(String theatherName) throws NoMatchingTheathersFoundException;
	Theather getTheather(String string) throws CannotFindTheatherException;

}
