package com.dc.movies.service.exception;

public class NoTheathresPlayingThisMovieException extends MoviesException
{


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//Constructor that accepts a message
    public NoTheathresPlayingThisMovieException(String message)
    {
       super(ErrorCode.INVALID_FIELD, message);
    }
    
    public NoTheathresPlayingThisMovieException()
    {
       super();
    }
    
	 public NoTheathresPlayingThisMovieException(String message, Throwable throwable) {
			super(ErrorCode.INVALID_FIELD, message, throwable);
		}
    
}
