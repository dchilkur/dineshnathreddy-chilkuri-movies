package com.dc.movies.service.exception;

public class NoMatchingMoviesFoundException  extends MoviesException {
	// Parameterless Constructor
	public NoMatchingMoviesFoundException() {
	}

	// Constructor that accepts a message
	public NoMatchingMoviesFoundException(String message) {
		super(ErrorCode.INVALID_FIELD, message);
	}
	
    public NoMatchingMoviesFoundException(String message, Throwable throwable) {
		super(ErrorCode.INVALID_FIELD, message, throwable);
	}

}

