package com.dc.movies.service.exception;

public class NoMoviesAvailableException extends MoviesException{
	// Parameterless Constructor
	public NoMoviesAvailableException() {
	}

	// Constructor that accepts a message
	public NoMoviesAvailableException(String message) {
		super(ErrorCode.INVALID_FIELD, message);
	}
	
	   public NoMoviesAvailableException(String message, Throwable throwable) {
			super(ErrorCode.INVALID_FIELD, message, throwable);
		}

}
