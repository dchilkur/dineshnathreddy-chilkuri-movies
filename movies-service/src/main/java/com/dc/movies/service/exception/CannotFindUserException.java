package com.dc.movies.service.exception;

public class CannotFindUserException extends MoviesException{ 
	
    //Parameterless Constructor
    public CannotFindUserException() {}

    //Constructor that accepts a message
    public CannotFindUserException(String message)
    {
       super(ErrorCode.INVALID_FIELD, message);
    }
    
    public CannotFindUserException(String message, Throwable throwable) {
		super(ErrorCode.INVALID_FIELD, message, throwable);
	}
    

}
