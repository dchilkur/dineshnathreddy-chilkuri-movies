package com.dc.movies.service;

import java.util.List;

import com.dc.movies.entity.Inventory;
import com.dc.movies.entity.Movie;
import com.dc.movies.entity.Theather;
import com.dc.movies.entity.impl.InventoryImpl;
import com.dc.movies.service.exception.CannotFindMovieException;
import com.dc.movies.service.exception.CannotFindTheatherException;
import com.dc.movies.service.exception.NoMoviesCurrentlyPlayingException;
import com.dc.movies.service.exception.NoTheathresPlayingThisMovieException;

public interface InventoryService {
	
	void updateAvailableMovieTickets(String theathre, String movie, int noOfTickets);
	List<Inventory> getMovieListings();
	List<Movie> getAllMovies(String theather_id) throws CannotFindMovieException, NoMoviesCurrentlyPlayingException;
	
	List<Inventory> getInventoryByTheater(String theather_id) throws CannotFindMovieException, NoMoviesCurrentlyPlayingException;
	List<Inventory> getInventoryByMovie(String movie_id) throws CannotFindMovieException, NoMoviesCurrentlyPlayingException;
	List<Theather> getAllTheathers(String movie_id) throws CannotFindTheatherException, NoTheathresPlayingThisMovieException;
	int addInventory(Inventory inventory);
	Inventory getInventory(Integer id);

}
