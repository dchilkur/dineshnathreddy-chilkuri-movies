package com.dc.movies.service.exception;

public class NotCurrentlyPlayingException extends MoviesException{
	// Parameterless Constructor
	
	//added a comment
	public NotCurrentlyPlayingException() {
	}

	// Constructor that accepts a message
	public NotCurrentlyPlayingException(String message) {
		super(ErrorCode.INVALID_FIELD, message);
	}
	
	 public NotCurrentlyPlayingException(String message, Throwable throwable) {
			super(ErrorCode.INVALID_FIELD, message, throwable);
		}

}
