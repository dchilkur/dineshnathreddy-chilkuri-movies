package com.dc.movies.service.exception;





public class CannotFindMovieException extends MoviesException
{


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//Constructor that accepts a message
    public CannotFindMovieException(String message)
    {
       super(ErrorCode.INVALID_FIELD, message);
    }
    
    public CannotFindMovieException(String message, Throwable throwable) {
		super(ErrorCode.INVALID_FIELD, message, throwable);
	}
    
    public CannotFindMovieException()
    {
       super();
    }
    
}
