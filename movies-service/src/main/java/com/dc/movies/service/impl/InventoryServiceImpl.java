package com.dc.movies.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dc.movies.entity.Inventory;
import com.dc.movies.entity.Movie;
import com.dc.movies.entity.Theather;
import com.dc.movies.repository.InventoryServiceRepository;
import com.dc.movies.repository.TheatherServiceRepository;
import com.dc.movies.service.InventoryService;
import com.dc.movies.service.MovieService;
import com.dc.movies.service.TheatherService;
import com.dc.movies.service.exception.CannotFindMovieException;
import com.dc.movies.service.exception.CannotFindTheatherException;
import com.dc.movies.service.exception.NoMoviesCurrentlyPlayingException;
import com.dc.movies.service.exception.NoTheathresPlayingThisMovieException;
import com.dc.movies.util.MovieUtil;

@Service
@Transactional
public class InventoryServiceImpl implements InventoryService {
	
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private MovieService movieService;

	@Autowired
	private TheatherService theatherService;
	
	@Autowired
	private InventoryServiceRepository inventoryServiceRepository;

	@Override
	public List<Inventory> getMovieListings() {
		
		List<Inventory> inventoryList = inventoryServiceRepository.getMovieListings();
		return inventoryList;
		// TODO Auto-generated method stub

	}

	@Override
	public void updateAvailableMovieTickets(String theathre, String movie,
			int noOfTickets) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Movie> getAllMovies(String theather_id)
			throws CannotFindMovieException, NoMoviesCurrentlyPlayingException {
		// TODO Auto-generated method stub

		List<Movie> moviesList = inventoryServiceRepository.getAllMovies(theather_id);
		return moviesList;
	}

	@Override
	public List<Theather> getAllTheathers(String movie_id)
			throws CannotFindTheatherException,
			NoTheathresPlayingThisMovieException {
		// TODO Auto-generated method stub


		List<Theather> theathersList =inventoryServiceRepository.getAllTheathers(movie_id);

		return theathersList;
	}

	@Override
	public int addInventory(Inventory inventory) {
		// TODO Auto-generated method stub
		logger.info("Adding Inventory - inventory service");
		return inventoryServiceRepository.addInventory(inventory);
	}

	@Override
	public Inventory getInventory(Integer id) {
		// TODO Auto-generated method stub
		return inventoryServiceRepository.getInventory(id);
	}

	@Override
	public List<Inventory> getInventoryByTheater(String theather_id)
			throws CannotFindMovieException, NoMoviesCurrentlyPlayingException {
		// TODO Auto-generated method stub
		List<Inventory> inventoryList = inventoryServiceRepository.getAllInventoryByTheather(theather_id);
		return inventoryList;
	}

	@Override
	public List<Inventory> getInventoryByMovie(String movie_id)
			throws CannotFindMovieException, NoMoviesCurrentlyPlayingException {
		// TODO Auto-generated method stub
		List<Inventory> inventoryList = inventoryServiceRepository.getAllInventoryByMovie(movie_id);
		return inventoryList;
	}



}
