package com.dc.movies.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dc.movies.entity.Theather;
import com.dc.movies.repository.TheatherServiceRepository;
import com.dc.movies.service.TheatherService;
import com.dc.movies.service.exception.CannotFindTheatherException;
import com.dc.movies.service.exception.NoMatchingTheathersFoundException;
import com.dc.movies.service.exception.NoTheathersAvailableException;

@Service
@Transactional
public class TheathreServiceImpl implements TheatherService {
	
	@Autowired
	private TheatherServiceRepository theatherServiceRepository;

	@Override
	public  String addTheather(Theather theather) {

		return (String) theatherServiceRepository.addTheather(theather);
		// TODO Auto-generated method stub

	}

	@Override
	public void removeTheather(Theather theather) {
		// TODO Auto-generated method stub
		
		theatherServiceRepository.removeTheather(theather);

	}

	@Override
	public void updateTheather(Theather theather) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Theather> getAllTheathres()
			throws NoTheathersAvailableException {
		// TODO Auto-generated method stub
		List<Theather> theathers = theatherServiceRepository.getAllTheathres();
	if(!(theathers.size()>0)){
			
			throw new NoTheathersAvailableException("No Theathers Available at this time");
			
		}
		return theathers;
	}

	@Override
	public List<Theather> getAllTheathres(String theatherName)
			throws NoMatchingTheathersFoundException {
		// TODO Auto-generated method stub
		List<Theather> theathers = theatherServiceRepository.getAllTheathres(theatherName);
		if(!(theathers.size()>0)){
			
			throw new NoMatchingTheathersFoundException("No Matching Theathers Found");
			
		}

		return theathers;

	}

	@Override
	public Theather getTheather(String theather_id)
			throws CannotFindTheatherException {
		// TODO Auto-generated method stub
		Theather theather = theatherServiceRepository.getTheather(theather_id);
		
		if(theather==null){
			
			throw new NoMatchingTheathersFoundException("No Matching Theathers Found");
			
		}
		return theather;
	}

}
