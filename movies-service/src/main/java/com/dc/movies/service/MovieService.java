package com.dc.movies.service;

import java.util.List;

import com.dc.movies.entity.Movie;
import com.dc.movies.service.exception.CannotFindMovieException;
import com.dc.movies.service.exception.NoMatchingMoviesFoundException;
import com.dc.movies.service.exception.NoMoviesAvailableException;


public interface MovieService {
	String addMovie(Movie movie);
	void removeMovie(Movie movie);
	void updateMovie(Movie movie);
	Movie getMovie(String movie_id) throws CannotFindMovieException;
	List<Movie> getAllMovies() throws NoMoviesAvailableException;
	List<Movie> getAllMovies(String movieName) throws NoMatchingMoviesFoundException;

}
