package com.dc.movies.service.exception;

public class NoTheathersAvailableException  extends MoviesException{
	// Parameterless Constructor
	public NoTheathersAvailableException() {
	}

	// Constructor that accepts a message
	public NoTheathersAvailableException(String message) {
		super(ErrorCode.INVALID_FIELD, message);
	}
	
	 public NoTheathersAvailableException(String message, Throwable throwable) {
			super(ErrorCode.INVALID_FIELD, message, throwable);
		}

}