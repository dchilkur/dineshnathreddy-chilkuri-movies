package com.dc.movies.service.impl;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.transaction.Transactional;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dc.movies.entity.PurchaseHistory;
import com.dc.movies.entity.User;
import com.dc.movies.entity.impl.UserImpl;
import com.dc.movies.repository.UserServiceRepository;
import com.dc.movies.service.UserService;



@Service
@Transactional
public class UserServiceImpl implements UserService {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	
	@Autowired
	private UserServiceRepository userServiceRepository;
	

	@Override
	public User getUser(String userId) {
		// TODO Auto-generated method stub
		
		return userServiceRepository.getUser(userId);
	}

	@Override
	public String registerUser(User usr) {
		// TODO Auto-generated method stub
		
		//let us hash the pin - TBTF bank does basic MD5
		com.dc.movies.entity.impl.UserImpl impl = (UserImpl)usr;
		impl.setPassword(md5base64(usr.getPassword()));		
		
		String user=userServiceRepository.registerUser(usr);
		return user;
	}

	@Override
	public PurchaseHistory getPreviousPurchaseHistory() {
		// TODO Auto-generated method stub
		return null;
	}
	
	private String md5base64(String pin){
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] digest = md.digest(pin.getBytes("UTF-8"));
			return Base64.encodeBase64String(digest);				
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			logger.error("failed to md5",e);
		}
		
		//TODO - this needs to be handled better
		throw new IllegalArgumentException("Server fail");
	}	


}
