package com.dc.movies.util;

import java.util.ArrayList;
import java.util.List;

import com.dc.movies.entity.Inventory;
import com.dc.movies.entity.Movie;
import com.dc.movies.entity.Theather;
import com.dc.movies.entity.impl.InventoryImpl;
import com.dc.movies.entity.impl.MovieImpl;
import com.dc.movies.entity.impl.TheathreImpl;

public class MovieUtil {
	
	public static void main(String[] args){
		

	}

	//populates default list of movies
	public static List<Movie> populateMovies() {
		List<Movie> movies = new ArrayList<Movie>();
		
		// Adds a Movie
		MovieImpl departedMovie;
		departedMovie = new MovieImpl();
		String departedMovieCast = "Leonardo Dicaprio|Matt Damon|Martin Socerrese" ;
		departedMovie.setMovieID("departed");
		departedMovie.setCast(departedMovieCast);
		departedMovie.setMovieName("Departed");
		movies.add(departedMovie);

		// Adds a Movie
		MovieImpl spidermanMovie;
		spidermanMovie = new MovieImpl();
		String spidermanMovieCast ="Tobey Mguire|Kirsten Dunst|Sam Raimi" ;
		spidermanMovie.setMovieID("spiderman");
		spidermanMovie.setCast(spidermanMovieCast);
		spidermanMovie.setMovieName("Spider Man");
		movies.add(spidermanMovie);

		// Adds a Movie
		MovieImpl spidermanMovie2;
		spidermanMovie2 = new MovieImpl();
		String spiderman2MovieCast ="Tobey Mguire|Kirsten Dunst|Sam Raimi" ;
		spidermanMovie2.setMovieID("spiderman2");
		spidermanMovie2.setCast(spiderman2MovieCast);
		spidermanMovie2.setMovieName("Spider Man2");
		movies.add(spidermanMovie2);

		//returns list of movies
		return movies;

	}

	//populates default list of theathers
	public static List<Theather> populateTheathers() {
		List<Theather> theathers = new ArrayList<Theather>();

		// Adds a Theater
		TheathreImpl amc_mercado;
		amc_mercado = new TheathreImpl();
		amc_mercado.setNoOfSeats(1000);
		amc_mercado.setTheathre_name("AMC Mercado");
		amc_mercado.setTheatherID("amc_mercado");
		amc_mercado
				.setTheathre_address("3111 Mission College Blvd, Santa Clara, CA 95054");
		amc_mercado.setNoOfScreens(20);

		theathers.add(amc_mercado);

		// Adds a Theater
		TheathreImpl amc_eastridge;
		amc_eastridge = new TheathreImpl();

		amc_eastridge.setNoOfSeats(1000);
		amc_eastridge.setTheathre_name("AMC EastRidge");
		amc_eastridge.setTheatherID("amc_eastridge");
		amc_eastridge
				.setTheathre_address("2190 Eastridge Loop, San Jose, CA 95122");
		amc_eastridge.setNoOfScreens(15);

		theathers.add(amc_eastridge);

		// Adds a Theater
		TheathreImpl century_20_great_mall;
		century_20_great_mall = new TheathreImpl();

		century_20_great_mall.setNoOfSeats(1000);
		century_20_great_mall.setTheathre_name("Century 20 Great Mall and XD");
		century_20_great_mall.setTheatherID("century_20_great_mall");
		century_20_great_mall
				.setTheathre_address("1010 Great Mall Drive, Milpitas, CA 95035");
		century_20_great_mall.setNoOfScreens(25);

		theathers.add(century_20_great_mall);

		//returns list of theathers
		return theathers;

	}
	
	public static List<Inventory> populateInventory(){
		
		
		
		List<Inventory> inventoryList=new ArrayList<Inventory>();
		
		// Adds a Movie
		MovieImpl departedMovie;
		departedMovie = new MovieImpl();
		String departedMovieCast = "Leonardo Dicaprio|Matt Damon|Martin Socerrese" ;
		departedMovie.setMovieID("departed");
		departedMovie.setCast(departedMovieCast);
		departedMovie.setMovieName("Departed");


		// Adds a Movie
		MovieImpl spidermanMovie;
		spidermanMovie = new MovieImpl();
		String spidermanMovieCast ="Tobey Mguire|Kirsten Dunst|Sam Raimi" ;
		spidermanMovie.setMovieID("spiderman");
		spidermanMovie.setCast(spidermanMovieCast);
		spidermanMovie.setMovieName("Spider Man");


		// Adds a Movie
		MovieImpl spidermanMovie2;
		spidermanMovie2 = new MovieImpl();
		String spiderman2MovieCast =  "Tobey Mguire|Kirsten Dunst|Sam Raimi" ;
		spidermanMovie2.setMovieID("spiderman2");
		spidermanMovie2.setCast(spiderman2MovieCast);
		spidermanMovie2.setMovieName("Spider Man2");
		
		// Adds a Theater
		TheathreImpl amc_eastridge;
		amc_eastridge = new TheathreImpl();

		amc_eastridge.setNoOfSeats(1000);
		amc_eastridge.setTheathre_name("AMC EastRidge");
		amc_eastridge.setTheatherID("amc_eastridge");
		amc_eastridge
				.setTheathre_address("2190 Eastridge Loop, San Jose, CA 95122");
		amc_eastridge.setNoOfScreens(15);



		// Adds a Theater
		TheathreImpl century_20_great_mall;
		century_20_great_mall = new TheathreImpl();

		century_20_great_mall.setNoOfSeats(1000);
		century_20_great_mall.setTheathre_name("Century 20 Great Mall and XD");
		century_20_great_mall.setTheatherID("century_20_great_mall");
		century_20_great_mall
				.setTheathre_address("1010 Great Mall Drive, Milpitas, CA 95035");
		century_20_great_mall.setNoOfScreens(25);
		
		// Adds a Theater
		TheathreImpl amc_mercado;
		amc_mercado = new TheathreImpl();
		amc_mercado.setNoOfSeats(1000);
		amc_mercado.setTheathre_name("AMC Mercado");
		amc_mercado.setTheatherID("amc_mercado");
		amc_mercado
				.setTheathre_address("3111 Mission College Blvd, Santa Clara, CA 95054");
		amc_mercado.setNoOfScreens(20);
		
		
		// Adds a Inventory Item
		InventoryImpl inventoryItem1=new InventoryImpl();
		inventoryItem1.setMovie(spidermanMovie);
		inventoryItem1.setTheather(amc_mercado);
		inventoryItem1.setPricePerTicket(10);
		inventoryItem1.setNumOfTicketsAvailable(100);
		inventoryItem1.setCurrentlyRunning(true);
		inventoryList.add(inventoryItem1);
		
		// Adds a Inventory Item
		InventoryImpl inventoryItem2=new InventoryImpl();
		inventoryItem2.setMovie(departedMovie);
		inventoryItem2.setTheather(amc_mercado);
		inventoryItem2.setPricePerTicket(12);
		inventoryItem2.setNumOfTicketsAvailable(120);
		inventoryItem2.setCurrentlyRunning(true);
		inventoryList.add(inventoryItem2);
		
		// Adds a Inventory Item
		InventoryImpl inventoryItem3=new InventoryImpl();
		inventoryItem3.setMovie(spidermanMovie2);
		inventoryItem3.setTheather(amc_mercado);
		inventoryItem3.setPricePerTicket(11);
		inventoryItem3.setNumOfTicketsAvailable(80);
		inventoryItem3.setCurrentlyRunning(true);
		inventoryList.add(inventoryItem3);
		
		
		// Adds a Inventory Item
		InventoryImpl inventoryItem4=new InventoryImpl();
		inventoryItem4.setMovie(spidermanMovie2);
		inventoryItem4.setTheather(century_20_great_mall);
		inventoryItem4.setPricePerTicket(11);
		inventoryItem4.setNumOfTicketsAvailable(80);
		inventoryItem4.setCurrentlyRunning(true);
		inventoryList.add(inventoryItem4);
		
		// Adds a Inventory Item
		InventoryImpl inventoryItem5=new InventoryImpl();
		inventoryItem5.setMovie(spidermanMovie);
		inventoryItem5.setTheather(century_20_great_mall);
		inventoryItem5.setPricePerTicket(10);
		inventoryItem5.setNumOfTicketsAvailable(100);
		inventoryItem5.setCurrentlyRunning(true);
		inventoryList.add(inventoryItem5);
		
		// Adds a Inventory Item
		InventoryImpl inventoryItem6=new InventoryImpl();
		inventoryItem6.setMovie(spidermanMovie2);
		inventoryItem6.setTheather(century_20_great_mall);
		inventoryItem6.setPricePerTicket(11);
		inventoryItem6.setNumOfTicketsAvailable(80);
		inventoryItem6.setCurrentlyRunning(true);
		inventoryList.add(inventoryItem6);
		
		// Adds a Inventory Item
		InventoryImpl inventoryItem7=new InventoryImpl();
		inventoryItem7.setMovie(departedMovie);
		inventoryItem7.setTheather(century_20_great_mall);
		inventoryItem7.setPricePerTicket(10);
		inventoryItem7.setNumOfTicketsAvailable(100);
		inventoryItem7.setCurrentlyRunning(true);
		inventoryList.add(inventoryItem7);
		
		//returning a list of inventory items
		return inventoryList;
		
	}

}
