package com.dc.movies.http;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
//import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dc.movies.entity.Movie;
import com.dc.movies.entity.impl.MovieImpl;
import com.dc.movies.http.entity.HttpMovie;
import com.dc.movies.service.MovieService;
import com.dc.movies.service.exception.CannotFindMovieException;

@Path("/movies")
@Component
@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
public class MovieResource {
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private MovieService movieService;

	@POST
	@Path("/")
	public Response createMovie(HttpMovie newMovie)
			throws CannotFindMovieException {

		Movie movieToCreate = convert(newMovie);
		String mID = movieService.addMovie(movieToCreate);
		Movie addedMovie = movieService.getMovie(mID);

		return Response.status(Status.CREATED)
				.header("Location", "/movie/" + addedMovie)
				.entity(new HttpMovie(addedMovie)).build();
	}

	@GET
	@Path("/")
	public List<HttpMovie> getallMovies() throws CannotFindMovieException {
		logger.info("Getting all Movies");

		List<Movie> movies = movieService.getAllMovies();

		List<HttpMovie> returnList = new ArrayList<>(movies.size());
		for (Movie movie : movies) {
			returnList.add(new HttpMovie(movie));
		}
		return returnList;
	}

	@GET
	@Path("/{movieId}")
	public HttpMovie geMovieById(@PathParam("movieId") String movieId)
			throws CannotFindMovieException {
		logger.info("getting movie by id:" + movieId);

		Movie movie = movieService.getMovie(movieId);
		return new HttpMovie(movie);
	}

	@GET
	@Path("/name/{moviePattern}")
	@Wrapped(element = "movies")
	public List<HttpMovie> geMovieByPattern(
			@PathParam("moviePattern") String moviePattern)
			throws CannotFindMovieException {
		logger.info("getting movie by moviePattern:" + moviePattern);

		List<Movie> movies = movieService.getAllMovies(moviePattern);

		List<HttpMovie> returnList = new ArrayList<>(movies.size());
		for (Movie movie : movies) {
			returnList.add(new HttpMovie(movie));
		}
		return returnList;

	}

	/**
	 * Not pushing this into business layer. Could be a util as well
	 * 
	 * @param newUser
	 * @return
	 */
	private Movie convert(HttpMovie httpMovie) {
		MovieImpl movie = new MovieImpl();
		movie.setMovieID(httpMovie.movieID);
		movie.setMovieName(httpMovie.movieName);
		SimpleDateFormat sf = new SimpleDateFormat("mm-dd-yyyy");

		try {
			movie.setRelease_date(sf.parse(httpMovie.releaseDate));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		movie.setCast(httpMovie.movieCast);

		return movie;
	}

}