package com.dc.movies.http.entity;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.dc.movies.entity.Inventory;
import com.dc.movies.entity.Movie;
import com.dc.movies.entity.Theather;



/**
 * Select fields we want exposed to the REST layer. Separation from business/data layer. 
 * 
 * Note "XML" annotation. The resteasy-jettison implementation converts these to JSON depending on
 * the Accept media type
 *
 */
@XmlRootElement(name = "inventory")
public class HttpInventory {
	
	@XmlElement
	public  int inventoryID;
	
	@XmlElement
	public  String movie;
	
	@XmlElement
	public  String theather;
	
	@XmlElement
	public  int pricePerTicket;
	
	@XmlElement
	public  int noOfTickets;
	
	@XmlElement
	public  boolean currently_running;
	

	
	//required by framework
	protected HttpInventory() {}

	public HttpInventory(Inventory inventory) {
		this.inventoryID=inventory.getID();
		this.movie=inventory.getMovie().getMovieName();
		this.theather=inventory.getTheather().getTheathreName();
		this.pricePerTicket =inventory.getPricePerTicket();
		this.noOfTickets=inventory.getNumOfTicketsAvailable();
		this.currently_running=inventory.getCurrentlyRunning();

	}
	
}
