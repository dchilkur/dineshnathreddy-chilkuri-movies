package com.dc.movies.http.entity;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.dc.movies.entity.Movie;



/**
 * Select fields we want exposed to the REST layer. Separation from business/data layer. 
 * 
 * Note "XML" annotation. The resteasy-jettison implementation converts these to JSON depending on
 * the Accept media type
 *
 */
@XmlRootElement(name = "movie")
public class HttpMovie {
	
	@XmlElement
	public  String movieID;
	
	@XmlElement
	public  String movieName;
	
	@XmlElement
	public  String movieCast;
	
	@XmlElement
	public  String releaseDate;
	

	
	//required by framework
	protected HttpMovie() {}

	public HttpMovie(Movie movie) {
		this.movieID=movie.getMovieId();
		this.movieName=movie.getMovieName();
		this.movieCast=movie.getMovieCast();
		this.releaseDate=movie.getRelease_date().toString();

	}
	
}
