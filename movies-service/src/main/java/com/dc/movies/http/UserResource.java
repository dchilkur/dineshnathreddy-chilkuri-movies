package com.dc.movies.http;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dc.movies.entity.User;
import com.dc.movies.entity.impl.UserImpl;
import com.dc.movies.http.entity.HttpUser;
import com.dc.movies.service.UserService;


@Path("/users")
@Component
@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
public class UserResource {
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private UserService userService;

	@POST
	@Path("/")
	public Response createUser(HttpUser user) {

		User userToCreate = convert(user);
		String userId = userService.registerUser(userToCreate);
		
		logger.info("user created is"+userId);
		User addedUser = userService.getUser(userId);

		return Response.status(Status.CREATED)
				.header("Location", "/user/" + addedUser)
				.entity(new HttpUser(addedUser)).build();
	}

	@GET
	@Path("/{userId}")
	public HttpUser getUserById(@PathParam("userId") String userID) {
		logger.info("getting user by id:" + userID);

		User user = userService.getUser(userID);
		return new HttpUser(user);
	}
	


	/**
	 * Not pushing this into business layer. Could be a util as well
	 * 
	 * @param newUser
	 * @return
	 */
	private User convert(HttpUser httpUser) {
		UserImpl user = new UserImpl();
		user.setUserName(httpUser.userName);
		user.setFirstName(httpUser.firstName);
		user.setLastName(httpUser.lastName);
		user.setPassword(httpUser.password);
		user.setRole(httpUser.role);

		return user;
	}

}