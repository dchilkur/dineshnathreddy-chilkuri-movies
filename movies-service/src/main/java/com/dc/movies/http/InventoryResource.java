package com.dc.movies.http;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dc.movies.entity.Inventory;
import com.dc.movies.entity.Movie;
import com.dc.movies.entity.Theather;
import com.dc.movies.entity.impl.InventoryImpl;
import com.dc.movies.http.entity.HttpInventory;
import com.dc.movies.http.entity.HttpMovie;
import com.dc.movies.http.entity.HttpTheather;
import com.dc.movies.service.InventoryService;
import com.dc.movies.service.MovieService;
import com.dc.movies.service.TheatherService;
import com.dc.movies.service.exception.CannotFindMovieException;
import com.dc.movies.service.exception.CannotFindTheatherException;
import com.dc.movies.service.exception.NoMoviesCurrentlyPlayingException;
import com.dc.movies.service.exception.NoTheathresPlayingThisMovieException;




@Path("/showtimes")
@Component
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class InventoryResource {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private InventoryService inventoryService;
	
	@Autowired
	private MovieService movieService;
	
	@Autowired
	private TheatherService theatherService;
	
	@POST
	@Path("/")
	public Response addInventory(HttpInventory inventory) throws CannotFindMovieException, CannotFindTheatherException{
		
		logger.info("in Add inventory method");
		Inventory inventoryToAdd = convert(inventory);
		int inventoryID=inventoryService.addInventory(inventoryToAdd);
		Inventory addedInventory = inventoryService.getInventory(inventoryID);
		
		return Response.status(Status.CREATED).header("Location", "/inventory/"+addedInventory).entity(new HttpInventory(addedInventory)).build();
	}	

	@GET
	@Path("/inventory/{inventoryID}")	
	public HttpInventory getInventoryById(@PathParam("inventoryID") int inventoryID) throws CannotFindMovieException{
		logger.info("getting inventory by id:"+inventoryID);
		
	
		Inventory inventory =inventoryService.getInventory(inventoryID);
		return new HttpInventory(inventory);
	}
	
	@GET
	@Path("/movie/{movieName}")
	@Wrapped(element="showtimes")
	public List<HttpTheather> getShowTimesByMovie(@PathParam("movieName") String movieName) throws CannotFindTheatherException, NoTheathresPlayingThisMovieException {


		

		List<Theather> theathers=inventoryService.getAllTheathers(movieName);
		
		List<HttpTheather> theathersList = new ArrayList<HttpTheather>(theathers.size());

		
		for(Theather theather:theathers){
			theathersList.add(new HttpTheather(theather));
		}
		return theathersList;
	}
	
	@GET
	@Path("/inventory/movie/{movieName}")
	@Wrapped(element="showtimes")
	public List<HttpInventory> getInventoryByMovie(@PathParam("movieName") String movieName) throws CannotFindTheatherException, NoTheathresPlayingThisMovieException {


		

		List<Inventory> inventory=inventoryService.getInventoryByMovie(movieName);
		
		List<HttpInventory> InventoryList = new ArrayList<HttpInventory>(inventory.size());

		for(Inventory i:inventory){
			InventoryList.add(new HttpInventory(i));
			
		}
		

		return InventoryList;
	}
	
	
	@GET
	@Path("/inventory/theather/{theatherName}")
	@Wrapped(element="showtimes")
	public List<HttpInventory> getInventoryByThather(@PathParam("theatherName") String theatherName) throws CannotFindMovieException, NoMoviesCurrentlyPlayingException {


		List<Inventory> inventory=inventoryService.getInventoryByTheater(theatherName);

		List<HttpInventory> InventoryList = new ArrayList<HttpInventory>(inventory.size());

		for(Inventory i:inventory){
			InventoryList.add(new HttpInventory(i));
			
		}
		

		return InventoryList;
	}
	
	@GET
	@Path("/theather/{theatherName}")
	@Wrapped(element="showtimes")
	public List<HttpMovie> getShowTimes(@PathParam("theatherName") String theatherName) throws CannotFindMovieException, NoMoviesCurrentlyPlayingException {


		
		List<Movie> movies=inventoryService.getAllMovies(theatherName);
		
		List<HttpMovie> moviesList = new ArrayList<HttpMovie>(movies.size());



		for(Movie movie:movies){
			moviesList.add(new HttpMovie(movie));
			
		}
		return moviesList;
	}

	
	/**
	 * Not pushing this into business layer. Could be a util as well
	 * @param newUser
	 * @return
	 * @throws CannotFindTheatherException 
	 * @throws CannotFindMovieException 
	 */
	private Inventory convert(HttpInventory httpInventory) throws CannotFindTheatherException, CannotFindMovieException {
		InventoryImpl inventory = new InventoryImpl();
		inventory.setPricePerTicket(httpInventory.pricePerTicket);
		inventory.setTheather(theatherService.getTheather(httpInventory.theather));
		inventory.setMovie(movieService.getMovie(httpInventory.movie));
		inventory.setId(httpInventory.inventoryID);
		inventory.setNumOfTicketsAvailable(httpInventory.noOfTickets);
		inventory.setCurrentlyRunning(httpInventory.currently_running);

		return inventory;
	}
	
}