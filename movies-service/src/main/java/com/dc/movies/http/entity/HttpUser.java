package com.dc.movies.http.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.dc.movies.entity.Role;
import com.dc.movies.entity.User;



/**
 * Select fields we want exposed to the REST layer. Separation from business/data layer. 
 * 
 * Note "XML" annotation. The resteasy-jettison implementation converts these to JSON depending on
 * the Accept media type
 *
 */
@XmlRootElement(name = "user")
public class HttpUser {
	
	@XmlElement
	public  String userName;
	
	@XmlElement
	public  String password;
	
	@XmlElement
	public  String firstName;
	
	@XmlElement
	public  String lastName;
	
	@XmlElement
	public  Role role;
	
	
	

	
	//required by framework
	protected HttpUser() {}

	public HttpUser(User user) {
		this.userName=user.getUserName();
		this.firstName=user.getFirstName();
		this.lastName=user.getLastName();
		this.role=user.getRole();

	}
	
}
