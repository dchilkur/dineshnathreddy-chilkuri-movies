package com.dc.movies.http;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dc.movies.entity.Movie;
import com.dc.movies.entity.Theather;
import com.dc.movies.entity.impl.TheathreImpl;
import com.dc.movies.http.entity.HttpMovie;
import com.dc.movies.http.entity.HttpTheather;
import com.dc.movies.service.TheatherService;
import com.dc.movies.service.exception.CannotFindMovieException;
import com.dc.movies.service.exception.CannotFindTheatherException;



@Path("/theathers")
@Component
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class TheatherResource {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private TheatherService theatherService;
	
	@POST
	@Path("/")
	public Response createTheather(HttpTheather newTheather) throws CannotFindMovieException, CannotFindTheatherException{
		Theather theatherToCreate = convert(newTheather);
		String theatherId=theatherService.addTheather(theatherToCreate);
		Theather addedTheather = theatherService.getTheather(theatherId);
		
		return Response.status(Status.CREATED).header("Location", "/theather/"+addedTheather).entity(new HttpTheather(addedTheather)).build();
	}	

	@GET
	@Path("/{theatherId}")	
	public HttpTheather getTheatherById(@PathParam("theatherId") String theatherId) throws CannotFindMovieException, CannotFindTheatherException{
		logger.info("getting theatehr by id:"+theatherId);
		
	
		Theather theather =theatherService.getTheather(theatherId);
		return new HttpTheather(theather);
	}
	
	
	@GET
	@Path("/name/{theatherPattern}")	
	@Wrapped(element="theathers")
	public List<HttpTheather> getTheatherByPattern(@PathParam("theatherPattern") String theatherPattern) throws CannotFindMovieException, CannotFindTheatherException{
		logger.info("getting theatehr by id:"+theatherPattern);
		
	
		List<Theather> theathers =theatherService.getAllTheathres(theatherPattern);
		
	
		
		List<HttpTheather> returnList = new ArrayList<>(theathers.size());
		for(Theather theather:theathers){
			returnList.add(new HttpTheather(theather));
		}
		return returnList;
	}
	

	
	/**
	 * Not pushing this into business layer. Could be a util as well
	 * @param newUser
	 * @return
	 */
	private Theather convert(HttpTheather httpTheather) {
		TheathreImpl theather = new TheathreImpl();
		theather.setTheatherID(httpTheather.theather_id);
		theather.setTheathre_name(httpTheather.theather_name);
		theather.setNoOfSeats(httpTheather.no_of_seats);
		theather.setTheathre_address(httpTheather.theathre_address);
		theather.setNoOfScreens(httpTheather.no_of_screens);

		return theather;
	}
	
}