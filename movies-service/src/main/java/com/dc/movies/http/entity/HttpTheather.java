package com.dc.movies.http.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.dc.movies.entity.Theather;



/**
 * Select fields we want exposed to the REST layer. Separation from business/data layer. 
 * 
 * Note "XML" annotation. The resteasy-jettison implementation converts these to JSON depending on
 * the Accept media type
 *
 */
@XmlRootElement(name = "theather")
public class HttpTheather {
	
	@XmlElement
	public  String theather_name;
	
	@XmlElement
	public  String theather_id;
	
	@XmlElement
	public  int no_of_seats;
	
	@XmlElement
	public  int no_of_screens;
	
	@XmlElement
	public  String theathre_address;
	

	
	//required by framework
	protected HttpTheather() {}

	public HttpTheather(Theather theather) {
		this.theather_name=theather.getTheathreName();
		this.theather_id=theather.getTheatherID();
		this.no_of_seats=theather.getNoOfSeats();
		this.no_of_screens=theather.getNumberOfScreens();
		this.theathre_address=theather.getAddress();

	}
	
}
