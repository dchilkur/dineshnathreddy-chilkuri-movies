package com.dc.movies.entity;

public interface User {
	
String getUserName();	
	String getFirstName();
	String getLastName();
	String getPassword();
	Role getRole();

}
