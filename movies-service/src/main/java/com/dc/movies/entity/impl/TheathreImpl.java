package com.dc.movies.entity.impl;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.dc.movies.entity.Theather;

@Entity
@Table(name="Theather")
public class TheathreImpl implements Theather, Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8479464190806226017L;





	public String getTheathre_address() {
		return theathre_address;
	}

	public void setTheathre_address(String theathre_address) {
		this.theathre_address = theathre_address;
	}


	public String getTheathre_name() {
		return theather_name;
	}

	public void setTheathre_name(String theathre_name) {
		this.theather_name = theathre_name;
	}

	public void setNoOfSeats(int noOfSeats) {
		this.noOfSeats = noOfSeats;
	}

	@Column(name="address")
	private String theathre_address;
	
	@Id
	@Column(name="theather_id")
	private String theatherID;
	
	



	public void setTheatherID(String theatherID) {
		this.theatherID = theatherID;
	}

	@Column(name="theather_name")
	private String theather_name;
	
	@Column(name="no_of_seats")
	private int noOfSeats;
	
	@Column(name="no_of_screens")
	private int noOfScreens;

	public int getNoOfScreens() {
		return noOfScreens;
	}

	public void setNoOfScreens(int noOfScreens) {
		this.noOfScreens = noOfScreens;
	}

	@Override
	public int getNumberOfScreens() {
		// TODO Auto-generated method stub
		return noOfScreens;
	}


	@Override
	public int getNoOfSeats() {
		// TODO Auto-generated method stub
		return noOfSeats;
	}

	@Override
	public String getAddress() {
		// TODO Auto-generated method stub
		return theathre_address;
	}

	@Override
	public String getTheathreName() {
		// TODO Auto-generated method stub
		return theather_name;
	}

	@Override
	public String getTheatherID() {
		// TODO Auto-generated method stub
		return theatherID;
	}


}
