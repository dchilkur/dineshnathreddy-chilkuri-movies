package com.dc.movies.entity.impl;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.dc.movies.entity.Inventory;
import com.dc.movies.entity.Movie;
import com.dc.movies.entity.Theather;

@Entity
@Table(name = "Inventory")
public class InventoryImpl implements Inventory, Serializable{ /**
	 * 
	 */
	private static final long serialVersionUID = -3542226471905107224L;



	@Id
	@Column(name = "inventory_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer inventory_id;

	
	@ManyToOne(targetEntity = TheathreImpl.class)
	@JoinColumn(name = "theather_theather_id")
	private Theather theather;
	
	
	@ManyToOne(targetEntity = MovieImpl.class)
	@JoinColumn(name = "movie_movie_id")
	private Movie movie;

	public void setTheather(Theather theather) {
		this.theather = theather;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}



	@Column(name = "no_of_tickets_available")
	private int numOfTicketsAvailable;

	@Column(name = "currently_running")
	private Boolean currentlyRunning;

	@Column(name = "ticket_price")
	private int pricePerTicket;

	@Override
	public int getNumOfTicketsAvailable() {
		return numOfTicketsAvailable;
	}

	public void setNumOfTicketsAvailable(int numOfTicketsAvailable) {
		this.numOfTicketsAvailable = numOfTicketsAvailable;
	}

	@Override
	public Boolean getCurrentlyRunning() {
		return currentlyRunning;
	}

	public void setCurrentlyRunning(Boolean currentlyRunning) {
		this.currentlyRunning = currentlyRunning;
	}

	@Override
	public int getPricePerTicket() {
		return pricePerTicket;
	}

	public void setPricePerTicket(int pricePerTicket) {
		this.pricePerTicket = pricePerTicket;
	}

	@Override
	public Integer getID() {
		// TODO Auto-generated method stub
		return inventory_id;
	}

	public int getId() {
		return inventory_id;
	}

	public void setId(int id) {
		this.inventory_id = id;
	}

	@Override
	public Movie getMovie() {
		// TODO Auto-generated method stub

		return movie;
	}

	@Override
	public Theather getTheather() {
		// TODO Auto-generated method stub
		return theather;
	}

}
