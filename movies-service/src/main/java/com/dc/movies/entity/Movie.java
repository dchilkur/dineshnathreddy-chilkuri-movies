package com.dc.movies.entity;


import java.util.Date;
import java.util.Map;

public interface Movie {
	
	String getMovieId();	
	String getMovieName();
	String getMovieCast();
	Date getRelease_date();
	
	
}
