package com.dc.movies.entity.impl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.dc.movies.entity.Role;
import com.dc.movies.entity.User;

@Entity
@Table(name = "User")
public class UserImpl implements User{

	@Id
	@Column(name="userName")
	private String userName;
	
	@Column(name="encryptedPassword")
	private String password;
	
	@Column(name="firstName")
	private String firstName;
	
	@Column(name="lastName")
	private String lastName;
	
	@Column(name="role")
	private Role role;

	//private Theather favouriteTheather;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
/*
	public Theather getFavouriteTheather() {
		return favouriteTheather;
	}

	public void setFavouriteTheather(Theather favouriteTheather) {
		this.favouriteTheather = favouriteTheather;
	}
*/

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


}
