package com.dc.movies.entity.impl;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.dc.movies.entity.Movie;

@Entity
@Table(name="Movie")
public class MovieImpl implements Movie, Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7069633201698453318L;

	@Column(name="movie_name")
	private String movie_name;
	
	@Column(name="movie_cast")
	private String cast;
	
	@Id
	@Column(name="movie_id")
	private String movie_id;
	
	@Column(name="release_date")
	@Type(type="date")
	private Date release_date;
	

	public Date getRelease_date() {
		return release_date;
	}
	public void setRelease_date(Date release_date) {
		this.release_date = release_date;
	}
	public String getMovieID() {
		return movie_id;
	}
	public void setMovieID(String movieID) {
		this.movie_id = movieID;
	}
	
	
	
	@Override
	public String getMovieId() {
		// TODO Auto-generated method stub
		return movie_id;
	}
	

	@Override
	public String getMovieCast() {
		// TODO Auto-generated method stub
		return cast;
	}

	
	@Override
	public String getMovieName() {
		return movie_name;
	}
	public void setMovieName(String movieName) {
		this.movie_name = movieName;
	}
	public String getCast() {
		return cast;
	}
	public void setCast(String movieCast) {
		this.cast = movieCast;
	}


}
