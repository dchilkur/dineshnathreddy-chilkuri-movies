package com.dc.movies.entity;

public interface Inventory {
	
	int getNumOfTicketsAvailable();
	Movie getMovie();
	Theather getTheather();
	Boolean getCurrentlyRunning();
	int getPricePerTicket();
	Integer getID();
	

}
