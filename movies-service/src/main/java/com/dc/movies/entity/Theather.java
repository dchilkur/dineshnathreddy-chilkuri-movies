package com.dc.movies.entity;

public interface Theather {
	int getNumberOfScreens();
	String getTheatherID();
	int getNoOfSeats();
    String getAddress();
    String getTheathreName();

}
