function CarouselDemoCtrl($scope) {
  $scope.myInterval = 5000;
  var slides = $scope.slides = [];
  $scope.addSlide = function(movieTitle, movieImage, movieDescription) {
    var newWidth = 600 + slides.length;
    slides.push({
      title: movieTitle,
      movieId:movieImage,
      image: 'img/' + movieImage + '.jpeg',
      text: movieDescription
    });
  };
 // for (var i=0; i<4; i++) {
    $scope.addSlide("Rio 2","rio2", "Is this movie right for your kids?");
    $scope.addSlide("Draft Day", "draft-day", "Draft Day Tickets and Song Download");
    $scope.addSlide("Argo","argo", "A Must Watch?");
//  }
  
  /*
   *   $scope.myInterval = 5000;
  var slides = $scope.slides = [];
  $scope.addSlide = function() {
    var newWidth = 600 + slides.length;
    slides.push({
      image: 'http://placekitten.com/' + newWidth + '/300',
      text: ['More','Extra','Lots of','Surplus'][slides.length % 4] + ' ' +
        ['Cats', 'Kittys', 'Felines', 'Cutes'][slides.length % 4]
    });
  };
  for (var i=0; i<4; i++) {
    $scope.addSlide();
  }
   */
}