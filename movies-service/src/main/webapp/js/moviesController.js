var moviesApp = angular.module('moviesApp', ['ngRoute','ui.bootstrap','ngCookies']);


moviesApp.config(function ($routeProvider){
	$routeProvider. 
	when('/',{
		controller: 'moviesController',
		templateUrl: 'partials/home.html'
	})
	.when('/login',{
		controller: 'moviesController',
		templateUrl: 'partials/login.html'
	})
	.when('/movies/:movieID',{
		controller: 'showTimesController',
		templateUrl: 'partials/movies.html'
	})
	.when('/theathers/:theatherID',{
		controller: 'moviesController',
		templateUrl: 'partials/theathers.html'
	})
	.when('/showtimes/movie/:showTimesByMovie',{
		controller: 'showTimesController',
		templateUrl: 'partials/showtimesByMovie.html'
	})
	.when('/showtimes/theather/:showTimesByTheather',{
		controller: 'showTimesController',
		templateUrl: 'partials/showTimesByTheather.html'
	})
	.otherwise({ redirectTo: '/'});
	
});


moviesApp.controller('moviesController', function($scope, $http,  $cookies, $window, $routeParams) {
	$scope.movies = [];


		$scope.messageText="";
		
		init();
		
		function init(){
			
			if($cookies.loggedin!=undefined){
			    $scope.userName = $cookies.loggedin;
			    $scope.showLogin= false;
			    $scope.showLogout= true;
				
			}else{
			    $scope.userName ="guest";
			    $scope.showLogin= true;
			    $scope.showLogout= false;
				
			}
			
			$scope.logout=function (){
				
				$cookies.loggedin = undefined;
				 $window.location.href = '/';
				
			};
			
		}
		
		//search
		
		
		
		

			$http.get('/rest/movies/')
			.success(function(data, status) {
				$scope.httpStatus = status;
				$scope.httpData = data;
				$scope.errorStatus=false;
				$scope.messageText="Found "+data.length;
				$scope.movies = data;

			})
			.error(function(data, status) {
				$scope.httpStatus = status;				
				$scope.httpData = data;
				$scope.errorStatus=true;
				$scope.messageText=data.error.code+ " "+ data.error.message;
			});	
		
		
		/**/
		
		if($routeParams.theatherID){
			//remove current results
			$scope.theaters = [];
			$scope.messageText="";
			
			
			
			//search
			$http.get('/rest/theathers/name/'+$routeParams.theatherID)
				.success(function(data, status) {
					$scope.httpStatus = status;
					$scope.httpData = data;
					$scope.errorStatus=false;
					$scope.messageText="Found "+data.length;
					$scope.theaters = data;
					
					
				})
				.error(function(data, status) {
					$scope.httpStatus = status;				
					$scope.httpData = data;
					$scope.errorStatus=true;
					$scope.messageText=data.error.code+ " "+ data.error.message;
				});	
		
		}
		
		/**/
		
		
		$scope.create = function() {
			$scope.messageText="";
			
			$http.post('/rest/users',
					{"user":{"userName":$scope.userName, "firstName":$scope.firstName,"lastName":$scope.lastName,"password":$scope.password, "role":"USER"}})
				.success(function(data, status) {
					$scope.httpStatus = status;
					$scope.httpData = data;
					$scope.errorStatus=false;
					$scope.messageText="Created new user with ID "+data.user.id;

					// Setting a cookie
				    $cookies.loggedin = $scope.userName;
				    
				    $window.location.href = '/';
				})
				.error(function(data, status) {
					$scope.httpStatus = status;				
					$scope.httpData = data;
					$scope.errorStatus=true;
					$scope.messageText=data.error.code+ " "+ data.error.message;
				});
		};
		
		
		$scope.getMovies = function(val) {
			
		    return $http.get('/rest/movies/name/'+val).then(function(res){

		      var movies = [];

		      angular.forEach(res.data, function(item){
		    	  movies.push(item.movie.movieID);
		      });

		      return movies;
		    });
		  };
		  

		$scope.getTheaters = function(val) {
		
				
			    return $http.get('/rest/theathers/name/'+val).then(function(res){

			      var theathers = [];

			      angular.forEach(res.data, function(item){
			    	  theathers.push(item.theather.theather_id);
			      });

			      return theathers;
			    });
			  };
		  

	
});


moviesApp.controller('showTimesController', function($scope, $http,  $cookies, $window, $routeParams) {
	if($routeParams.movieID){
		//remove current results
		$scope.movies = [];
		$scope.messageText="";
		
		
		
		//search
		$http.get('/rest/movies/name/'+$routeParams.movieID)
			.success(function(data, status) {
				$scope.httpStatus = status;
				$scope.httpData = data;
				$scope.errorStatus=false;
				$scope.messageText="Found "+data.length;
				$scope.movies = data;
				
				
			})
			.error(function(data, status) {
				$scope.httpStatus = status;				
				$scope.httpData = data;
				$scope.errorStatus=true;
				$scope.messageText=data.error.code+ " "+ data.error.message;
			});	
	
	}
	
	if($routeParams.showTimesByMovie){
		//remove current results
		$scope.theaters = [];
		$scope.messageText="";
		
		
		
		//search
		$http.get('/rest/showtimes/movie/'+$routeParams.showTimesByMovie)
			.success(function(data, status) {
				$scope.httpStatus = status;
				$scope.httpData = data;
				$scope.errorStatus=false;
				$scope.messageText="Found "+data.length;
				$scope.theaters = data;
				
				
			})
			.error(function(data, status) {
				$scope.httpStatus = status;				
				$scope.httpData = data;
				$scope.errorStatus=true;
				$scope.messageText=data.error.code+ " "+ data.error.message;
			});	
	
	
	}
	if($routeParams.showTimesByTheather){
		
		//search
		$http.get('/rest/showtimes/theather/'+$routeParams.showTimesByTheather)
			.success(function(data, status) {
				$scope.httpStatus = status;
				$scope.httpData = data;
				$scope.errorStatus=false;
				$scope.messageText="Found "+data.length;
				$scope.movies = data;
				
				
			})
			.error(function(data, status) {
				$scope.httpStatus = status;				
				$scope.httpData = data;
				$scope.errorStatus=true;
				$scope.messageText=data.error.code+ " "+ data.error.message;
			});	
		
	}
	

});







