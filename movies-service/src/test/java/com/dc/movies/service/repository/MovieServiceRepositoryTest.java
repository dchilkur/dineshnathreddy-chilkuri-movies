package com.dc.movies.service.repository;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import com.dc.movies.entity.Movie;
import com.dc.movies.entity.impl.MovieImpl;
import com.dc.movies.repository.MovieServiceRepository;
import com.dc.movies.service.exception.CannotFindMovieException;
import com.dc.movies.service.exception.NoMatchingMoviesFoundException;
import com.dc.movies.service.exception.NoMoviesAvailableException;

@ContextConfiguration(locations = {"classpath:spring-context.xml"})
public class MovieServiceRepositoryTest extends AbstractTransactionalJUnit4SpringContextTests{

	@Autowired
	private MovieServiceRepository movieServiceRepository;
	

	@Ignore
	public void testAddAndRetrieveMovie() throws CannotFindMovieException {

		// Adds a Movie
		MovieImpl departedMovie;
		departedMovie = new MovieImpl();
		String departedMovieCast =  "Leonardo Dicaprio|Matt Damon|Martin Socerrese" ;
		departedMovie.setMovieID("departed");
		departedMovie.setCast(departedMovieCast);
		departedMovie.setMovieName("Departed");
		String movie_id=movieServiceRepository.addMovie(departedMovie);
		Assert.assertNotNull(movie_id);
		
		Movie movie=movieServiceRepository.getMovie(movie_id);
		Assert.assertEquals("Incorrect movie title", "Departed", movie.getMovieName());
		
	}


	@Test(expected = com.dc.movies.service.exception.CannotFindMovieException.class)
	public void testGetMovieException() throws CannotFindMovieException {

		Movie movie = movieServiceRepository.getMovie("avengers");
		Assert.assertEquals("Movie name is not correct", "Avengers",
				movie.getMovieName());

	}

	@Ignore
	public void testGetAllMovies() throws NoMoviesAvailableException {

		List<Movie> movies;

		movies = movieServiceRepository.getAllMovies();

		Assert.assertNotNull("No Movies Available in the System", movies);
	}
	
	@Test
	public void testGetAllMatchingMovies()
			throws NoMatchingMoviesFoundException {

		List<Movie> movies;

		movies = movieServiceRepository.getAllMovies("Spider Man");
		Assert.assertNotNull("No Matching Movies Available in the System",
				movies);
	}




}
