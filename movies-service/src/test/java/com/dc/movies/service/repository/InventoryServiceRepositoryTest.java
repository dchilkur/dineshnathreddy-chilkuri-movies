package com.dc.movies.service.repository;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.util.Assert;

import com.dc.movies.entity.Inventory;
import com.dc.movies.entity.Movie;
import com.dc.movies.entity.impl.InventoryImpl;
import com.dc.movies.repository.InventoryServiceRepository;
import com.dc.movies.service.MovieService;
import com.dc.movies.service.TheatherService;
import com.dc.movies.service.exception.CannotFindMovieException;
import com.dc.movies.service.exception.CannotFindTheatherException;

@ContextConfiguration(locations = {"classpath:spring-context.xml"})
public class InventoryServiceRepositoryTest extends AbstractTransactionalJUnit4SpringContextTests{
	
	@Autowired
	private InventoryServiceRepository inventoryServiceRepository;
	
	@Autowired
	private TheatherService theatherService;
	
	@Autowired
	private MovieService movieService;
	
	@Ignore
	public void testAddPlusGetInventory() throws CannotFindMovieException, CannotFindTheatherException{
		
		InventoryImpl inventory=new InventoryImpl();
		Movie departed=movieService.getMovie("departed");
		inventory.setMovie(departed);
		inventory.setTheather(theatherService.getTheather("amc_mercado"));
		inventory.setPricePerTicket(10);
		inventory.setNumOfTicketsAvailable(20);		
		inventory.setCurrentlyRunning(true);
		
		inventoryServiceRepository.addInventory(inventory);
		

		
	}
	
	@Test
	public void testGetInventory() throws CannotFindMovieException, CannotFindTheatherException{
		
		
		Inventory inventory=inventoryServiceRepository.getInventory(1);
		Assert.notNull(inventory);
		

		
	}
	

}
