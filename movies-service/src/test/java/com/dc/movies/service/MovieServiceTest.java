package com.dc.movies.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

import com.dc.movies.entity.Movie;
import com.dc.movies.entity.impl.MovieImpl;
import com.dc.movies.repository.MovieServiceRepository;
import com.dc.movies.service.exception.CannotFindMovieException;
import com.dc.movies.service.exception.NoMatchingMoviesFoundException;
import com.dc.movies.service.exception.NoMoviesAvailableException;

@ContextConfiguration(locations = {"classpath:spring-context.xml"})
public class MovieServiceTest  extends AbstractJUnit4SpringContextTests   {

	@Autowired
	private MovieService movieService;
	

	@Ignore
	public void testAddAndRetrieveMovie() throws CannotFindMovieException {

		// Adds a Movie
		MovieImpl departedMovie;
		departedMovie = new MovieImpl();
		String departedMovieCast =  "Leonardo Dicaprio|Matt Damon|Martin Socerrese" ;
		departedMovie.setMovieID("departed");
		departedMovie.setCast(departedMovieCast);
		departedMovie.setMovieName("Departed");
		String movie_id=movieService.addMovie(departedMovie);
		Assert.assertNotNull(movie_id);
		
		Movie movie=movieService.getMovie(movie_id);
		Assert.assertEquals("Incorrect movie title", "Departed", movie.getMovieName());
		
	}


	@Test(expected = com.dc.movies.service.exception.CannotFindMovieException.class)
	public void testGetMovieException() throws CannotFindMovieException {

		Movie movie = movieService.getMovie("avengers");
		Assert.assertEquals("Movie name is not correct", "Avengers",
				movie.getMovieName());

	}

	@Test
	public void testGetAllMatchingMovies()
			throws NoMatchingMoviesFoundException {

		List<Movie> movies;

		movies = movieService.getAllMovies("spiderman");
		Assert.assertNotNull("No Matching Movies Available in the System",
				movies);
	}

	@Test(expected = com.dc.movies.service.exception.NoMoviesAvailableException.class)
	public void testGetAllMatchingMoviesException()
			throws NoMatchingMoviesFoundException {

		List<Movie> movies;

		movies = movieService.getAllMovies("Avengers");
		Assert.assertNotNull("No Matching Movies Available in the System",
				movies);
	}

}
