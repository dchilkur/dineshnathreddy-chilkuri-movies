package com.dc.movies.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.dc.movies.entity.Theather;
import com.dc.movies.entity.impl.TheathreImpl;
import com.dc.movies.service.exception.CannotFindTheatherException;
import com.dc.movies.service.exception.NoMatchingTheathersFoundException;
import com.dc.movies.service.exception.NoTheathersAvailableException;

@ContextConfiguration(locations = { "classpath:spring-context.xml" })
public class TheathreServiceTest extends AbstractJUnit4SpringContextTests {

	@Autowired
	private TheatherService theatherService;
	
	@Ignore
	public void testAddAndRetrieveTheather() throws NoMatchingTheathersFoundException, CannotFindTheatherException{
		
		// Adds a Theater
		TheathreImpl amc_mercado;
		amc_mercado = new TheathreImpl();
		amc_mercado.setNoOfSeats(1000);
		amc_mercado.setTheathre_name("AMC Mercado");
		amc_mercado.setTheatherID("amc_mercado");
		amc_mercado
				.setTheathre_address("3111 Mission College Blvd, Santa Clara, CA 95054");
		amc_mercado.setNoOfScreens(20);
		String theather_id=theatherService.addTheather(amc_mercado);
		
		Assert.assertNotNull(theather_id);
		
		Theather theather=theatherService.getTheather(theather_id);
		Assert.assertEquals("Incorrect Theather Name", "AMC Mercado", theather.getTheathreName());
		
	}



	@Test(expected = com.dc.movies.service.exception.CannotFindTheatherException.class)
	public void testGetTheaterException() throws CannotFindTheatherException {

		Theather theather = theatherService.getTheather("century_50");

		Assert.assertEquals("Cannot find the Associated Theather",
				"Century 50", theather.getTheathreName());

	}



	@Test
	public void testGetAllMatchingTheathers()
			throws NoMatchingTheathersFoundException {

		List<Theather> theathers;

		theathers = theatherService.getAllTheathres("AMC");
		Assert.assertNotNull("No Matching Theathers Available in the System",
				theathers);
	}

	@Test(expected = com.dc.movies.service.exception.NoMatchingTheathersFoundException.class)
	public void testGetAllMatchingTheathersException()
			throws NoMatchingTheathersFoundException {

		List<Theather> theathers;

		theathers = theatherService.getAllTheathres("Regal");

		Assert.assertNotNull("No Matching Theathers Available in the System",
				theathers);

	}

}
