package com.dc.movies.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import com.dc.movies.entity.Inventory;
import com.dc.movies.entity.Movie;
import com.dc.movies.entity.Theather;
import com.dc.movies.entity.impl.InventoryImpl;
import com.dc.movies.service.exception.CannotFindMovieException;
import com.dc.movies.service.exception.CannotFindTheatherException;
import com.dc.movies.service.exception.NoMoviesCurrentlyPlayingException;
import com.dc.movies.service.exception.NoTheathresPlayingThisMovieException;

@ContextConfiguration(locations = { "classpath:spring-context.xml" })
public class InventoryServiceTest extends AbstractJUnit4SpringContextTests {

	@Autowired
	private InventoryService inventoryService;
	
	@Autowired
	private TheatherService theatherService;
	
	@Autowired
	private MovieService movieService;

	
	@Ignore
	public void testAddAndRetrieveInventory() throws CannotFindMovieException, CannotFindTheatherException{
		
		InventoryImpl inventory=new InventoryImpl();
		Movie departed=movieService.getMovie("departed");
		inventory.setMovie(departed);
		inventory.setTheather(theatherService.getTheather("amc_mercado"));
		inventory.setPricePerTicket(10);
		inventory.setNumOfTicketsAvailable(20);		
		inventory.setCurrentlyRunning(true);
		
		Integer inventory_id=inventoryService.addInventory(inventory);
		Assert.assertNotNull(inventory_id);
		
		Inventory inventory1=inventoryService.getInventory(inventory_id);
		Assert.assertEquals("Not Equal", inventory_id, inventory1.getID());
		
		
	}
	
	@Test
	public void testGetAllTheathersPlayingMovie()
			throws CannotFindTheatherException,
			NoTheathresPlayingThisMovieException {
		List<Theather> theaters = new ArrayList<Theather>();
		theaters = inventoryService.getAllTheathers("departed");

		Assert.assertNotNull("No Theathers are currently playing this movie",
				theaters);


	}

	@Test
	public void testGetAllMoviesCurrentlyPlaying()
			throws CannotFindMovieException, NoMoviesCurrentlyPlayingException {
		List<Movie> movies = new ArrayList<Movie>();
		movies = inventoryService.getAllMovies("amc_mercado");

		Assert.assertNotNull("No Movies Currently Playing this theather",
				movies);
	}
	
	@Test(expected = com.dc.movies.service.exception.NoMoviesCurrentlyPlayingException.class)
	public void testGetAllMoviesCurrentlyPlayingException() throws CannotFindMovieException, NoMoviesCurrentlyPlayingException{
		List<Movie> movies = new ArrayList<Movie>();
		movies = inventoryService.getAllMovies("regal_new_albany_stadium_16");
		Assert.assertTrue("Must not have any movies at this theather",
				movies.size() == 0);
		
	}
	
	@Test(expected = com.dc.movies.service.exception.NoTheathresPlayingThisMovieException.class)
	public void testGetAllTheathersPlayingMovieException()
			throws CannotFindTheatherException,
			NoTheathresPlayingThisMovieException {
		List<Theather> theaters = new ArrayList<Theather>();
		theaters = inventoryService.getAllTheathers("hunger_games");

		Assert.assertTrue("Must not have any theathres playing this movie",
				theaters.size() == 0);

	}
}
